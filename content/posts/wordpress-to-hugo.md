---
title: "WordPress to Hugo"
date: 2019-03-17T14:00:00Z
draft: false
---

If you are reading this, then I have finally gotten round to migrating my blog to use the [Hugo static site generator](https://gohugo.io/). Previously I was using [WordPress](https://wordpress.org/) but I have been keen to migrate away from this for a long time due to it being overly complicated for what this site requires as well as the ongoing maintenance requirements for a WordPress site to keep it up-to-date and secure. WordPress is well known for having security vulnerabilities and has a much larger attack surface when compared with a static site.

The advantage a static site generators such as Hugo has is that the generated site is just static HTML files which need to be served by a web server. In the case of Hugo, editing content is performed offline by modifying plain text files written in markdown with YAML metadata which is compiled into a static HTML site with the help of a theme by the command line utility. Because the source for the site is stored in plain text files, it's easy to use the same version control tools that would be used for software projects.

Even if you automate the updates of the WordPress site, you can run into problems and additional work when a plugin being used is not compatible with the new version or there is another breaking change as part of the update and this creates an additional maintenance overhead. In comparison, keeping a statically generated site up to date is considerably easier as it mostly boils down to keeping the web server up to date which is less tightly coupled to the site content and as such it is unlikely that performing these tasks would cause a problem.

Of course, the limitation of a static site is that visitors to the site have no way to interact with the content, right? While it is true that visitors to a typical statically generated site won't be able to have any bespoke interactions, there are still ways to make the content interactive. Using javascript or iframes, the client can render dynamic content served by external sources and third-parties. Where the site is one like this where it is essentially just a hobby, using client side includes and utilising third-party tools moves the burden of securing and maintaining the dynamic parts of the site to a company who's business interests align with maintaining it.

Hugo has inbuilt support for [Disqus](https://disqus.com/) and this is one example of how interactivity can provided in a static site. [Configuration](https://gohugo.io/content-management/comments/#configure-disqus) is as simple as creating a site on Disqus and adding the short name to the configuration file (assuming that the theme being used supports it).

A static site generator isn't right for every blog though, for a non-technical person updating a statically generated site can be a lot more complex and might not work well for a site that is regularly updated. For these scenarios a typical CMS can work better.

Whether Hugo is the right choice for this site is yet to be seen but the simplicity of use appealed to me. The other (arguably more) popular choice for blogs is [Jekyll](https://jekyllrb.com/).

Initially this will be starting again as a blank slate but over time I may choose to migrate content from my old blog here as well as hopefully find the time to start blogging about new and interesting projects that I am working on.
